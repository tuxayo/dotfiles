call plug#begin('~/.vim/plugged')
Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-rails'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-cucumber'
Plug 'scrooloose/nerdtree'
Plug 'rking/ag.vim'
Plug 'slim-template/vim-slim'
Plug 'scrooloose/syntastic'
Plug 'kchmck/vim-coffee-script'
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating' " dep of orgmode
"Plug 'mfukar/robotframework-vim'
"Plug 'vim-scripts/LanguageTool'
Plug 'rhysd/vim-grammarous'
call plug#end()


" Global settings
:set nocompatible
:set autoread
:set hlsearch
:highlight Normal ctermfg=Darkgreen ctermbg=Black
" :set cursorline
syntax enable
set background=dark
colorscheme default
set mouse=a " enable mouse in terminal
" ↓ was useful with solarized colorscheme but could be useful with others
" let g:solarized_termcolors=256 " improve a bit colors in terminal

" Softtabs, 2 spaces
set tabstop=2
set shiftwidth=2
set shiftround
set expandtab

" case insensitive search, expect when searching a word with upper case chars
set ignorecase
set smartcase


" disable vertical bar cursor in GVim, was confusing with command line Vim
set guicursor=i:block-Cursor

" Show line number
:set number
:highlight LineNr term=bold ctermfg=darkgray guifg=darkgray

" Special configuration for development
:filetype on
:autocmd FileType .c,.cpp,.cxx,.h set cindent|set cin|set softtabstop=4|set expandtab
:set autoindent

:autocmd FileType make setlocal noexpandtab

" Remove trailing whitespaces on save
" autocmd BufWritePre * :%s/\s\+$//e

" future AG binding: bind K to search word under cursor
"nnoremap K :Ag "\b<C-R><C-W>\b"<CR>:cw<CR>

" highlight 'en space'U+2002
au VimEnter,BufWinEnter * syn match ErrorMsg " "


" Highlight Tabs and Spaces
":highlight Tab ctermbg=darkgray guibg=darkgray
":highlight Space ctermbg=darkblue guibg=darkblue
":au BufWinEnter * let w:m2=matchadd('Tab', '\t', -1)
":au BufWinEnter * let w:m3=matchadd('Space', '\s\+$\| \+\ze\t', -1)
":set list listchars=tab:Â».,trail

" leader key is the default one (\)

" vimrc
nnoremap <leader>ev :tabe $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>


noremap <leader>p :CtrlPBuffer<CR>
noremap <leader>f :NERDTreeFind<CR>
noremap <leader>n :NERDTreeToggle<CR>

" Close the current buffer and move to the previous one
" <BAR> is |
nmap <leader>bq :bp <BAR> bd #<CR>

" save with ctrl-s
inoremap <C-s> <Esc>:w<CR>
noremap  <C-s> :w<CR>

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" copy and paste to system clipboard (gvim and vim with X support)
vnoremap <leader>c "+y
nnoremap <leader>v "+p
" for insert mode, go one char forward(l) to compensate the move inducted by
" switching from insert mode to normal mode
" inoremap <leader>v <Esc>l"+p " disabled because can't work consitently with
" it's normal mode counterpart without failling complely to work when cursor
" is on EOL

" move lines like alt in eclipse or gedit
vnoremap <A-k> xkP`[V`]
vnoremap <A-j> xp`[V`]

" change indentation
vnoremap <A-h> <gv
vnoremap <A-l> >gv

" Tab will insert tab at beginning of line,
" will use completion if not at beginning
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction
inoremap <Tab> <c-r>=InsertTabWrapper()<cr>

" change tab
noremap <A-PageUp> :tabp<CR>
noremap <A-PageDown> :tabn<CR>
" open/close tabs
noremap  <C-Insert> :tabnew<CR>
inoremap <C-Insert> <Esc>:tabnew<CR>
noremap  <C-Delete> :tabclose<CR>
inoremap <C-Delete> <Esc>:tabclose<CR>
" move tabs
noremap <C-S-PageUp> :tabm -1<CR>
noremap <C-S-PageDown> :tabm +1<CR>

" Delete/close buffer without closing the window
" https://superuser.com/questions/289285/how-to-close-buffer-without-closing-the-window/370121#370121
command Bd bp | sp | bn | bd

" don't use arrows in normal mode!!!
nnoremap <Left> :echoe "Use h you NOOB!"<CR>
nnoremap <Right> :echoe "Use l you NOOB!"<CR>
nnoremap <Up> :echoe "Use k you NOOB!"<CR>
nnoremap <Down> :echoe "Use j you NOOB!"<CR>
" same thing in insert mode!!!
inoremap <Left> <nop>
inoremap <Right> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>

" replace <esc> by jj to leave insert mode
" inoremap jj <esc>
" inoremap <special> <esc> <C-S-o>:echo "Use jj instead of escape you NOOB!"<CR>
" inoremap <esc>^[ <esc>^[


" for CtrlP to ignore folders
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,sip-communicator.bin

" syntastic
" TODO use those without breaking the status line
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_compiler_options = ' -std=c++11'

""" COMANDS AND FUNCTIONS """
" cucumber: open step definition in new tab with the command ":Step"
:command! Step normal <C-w>d<C-w><S-t>

" Move with hjkl while on insert mode using Ctrl
" :imap <C-h> <C-o><C-Left>
" :imap <C-j> <C-o>j
" :imap <C-k> <C-o>k
" :imap <C-l> <C-o><C-Right>
":imap <A-h> <C-o>h> " they are not applied
":imap <A-l> <C-o>l

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

" Merge a tab into a split in the previous window
function! MergeTabs()
  if tabpagenr() == 1
    return
  endif
  let bufferName = bufname("%")
  if tabpagenr("$") == tabpagenr()
    close!
  else
    close!
    tabprev
  endif
  split
  execute "buffer " . bufferName
endfunction
nmap <C-W>u :call MergeTabs()<CR>
