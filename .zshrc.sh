debug=false
$debug && echo "begin zshrc"

source ~/.config/dotfiles/custom_env_vars_and_shell_functions.sh

alias pkrs='pikaur -Rs'
alias pks='pikaur -S'
alias pacman-clean='sudo pacman -Rsn $(pacman -Qdtq)'

alias ec='emacsclient -c -a ""'

alias pingt='ping tuxayo.net'
alias pingf='ping -i0.2 www.fdn.org'
alias pingi='ping -i0.2 80.67.169.40'
alias pingg='ping $(ip ro | grep -v "dev tun" | grep default | cut -d " " -f3)' # gateway

alias grep='grep --color=always'
alias o='xdg-open'

alias lla='ls -lah'

alias dev='cd ~/../datumoj/dev'
alias gst='git status'
alias gd='git diff'
alias gds='git diff --staged'
alias gdh='git diff HEAD'
alias gl='git log'
alias glo='git log --oneline'
alias gb='git branch'
alias gc='git checkout'
alias ga='git add'
alias gco='git commit'
alias gf='git fetch'
alias gpl='git pull'
alias gph='git push'
alias gm='git merge'
alias gcp='git cherry-pick'
alias gsh='git show --word-diff'
alias gr='git restore'
alias gsw='git switch'


alias cl='cd $CLOUD_DIR'
alias dev='cd ~/../datumoj/dev'

if $(on_laptop); then
  alias ap='sudo create_ap wlp3s0 enp0s25 wifi_gratuit_mdp_123456789 123456789'
  alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0'

  # laboro
  export SYNC_REPO=/home/datumoj/dev/koha/koha-code
  export KTD_HOME=/home/datumoj/dev/koha/koha-testing-docker
  export LOCAL_USER_ID=$(id -u)
  source /home/datumoj/dev/koha/koha-testing-docker/files/bash_aliases
  alias ktd='bin/ktd'
else
fi

check_if_program_installed() {
  type $1 >/dev/null
}
alias if_installed=check_if_program_installed

export VISUAL="vim"

# ruby stuff
#PATH="`ruby -e 'puts Gem.user_dir'`/bin:$PATH"
#$debug && echo "begin ruby"
#alias be='bundle exec'
#alias sp='spring'
#export GEM_HOME=$(ruby -e 'print Gem.user_dir')
#if_installed rbenv && eval "$(rbenv init -)"
#$debug && echo "end ruby"

# python stuff
#$debug && echo "begin python"
export WORKON_HOME=$HOME/.local/share/virtualenvs
#source /usr/bin/virtualenvwrapper.sh
#$debug && echo "end python"

# ocaml stuff
if_installed opam && . /home/victor/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# zsh only stuff
if [ "$(basename $SHELL)" '==' "zsh" ]; then
  # any command preceded a space won't be saved, nice for passwords
  setopt HIST_IGNORE_SPACE
  setopt INTERACTIVE_COMMENTS # to use comments in terminal
  setopt HIST_SAVE_NO_DUPS # no duplicates in history
  setopt SHARE_HISTORY # share history between terminals

  HISTSIZE=100000
  SAVEHIST=100000
  # vim mode
  bindkey -v

  # reenable ctrl-r to search in command history
  # vim mode disabled it
  bindkey "^R" history-incremental-search-backward

  # edit current command in vim which is awesome for complex stuff
  bindkey "^V" edit-command-line

  zstyle ':prezto:load' pmodule \
    ''
  export FZF_DEFAULT_OPTS=--exact
  source /usr/share/fzf/key-bindings.zsh
  source /usr/share/fzf/completion.zsh
  [[ -e "/usr/share/fzf/fzf-extras.zsh" ]] \
  && source /usr/share/fzf/fzf-extras.zsh

fi
$debug && echo "end zshrc_____________________________"
