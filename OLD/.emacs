
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa-stable" . "http://stable.melpa.org/packages/" )
                         ("melpa" . "http://melpa.org/packages/")))

(setq paradox-github-token "5cd780d30b1146d2dfbfbdbb2b31e5e6d87a10dc")

(add-to-list 'load-path (locate-user-emacs-file "lisp/"))

;; Auto complete plugin
(require 'auto-complete-config)
(ac-config-default)

;; Add opam emacs directory to the load-path
;;(setq opam-share (substring
;;(shell-command-to-string "opam config var share 2> /dev/null") 0 -1)
;;)
;;(setq opam-bin (substring
;;(shell-command-to-string "opam config var bin 2> /dev/null") 0 -1)
;;)

;;(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
;;(add-to-list 'load-path (concat opam-share "/tuareg"))
;;(add-to-list 'exec-path opam-bin)

;; Load tuareg
;;(load "tuareg-site-file")

;; Load ocp-indent
;;(require 'ocp-indent)

;; Load merlin
;;(require 'merlin)

;; HASKELL______________________________________________________________________

;;haskell mode
(require 'package)
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
;; Ignore compiled Haskell files in filename completions
(add-to-list 'completion-ignored-extensions ".hi")

;; Plug hslint to Flymake to lint haskell code
(require 'hs-lint) ;; https://gist.github.com/1241059
(require 'haskell-ac) ;; https://gist.github.com/1241063

(defun flymake-haskell-init ()
  "When flymake triggers, generates a tempfile containing the contents of the current buffer, runs hslint on it, and deletes file. Put this file path (and run chmod a+x hslint) to enable hslint: https://gist.github.com/1241073"
  (let*
    (
      (temp-file (flymake-init-create-temp-buffer-copy 'flymake-create-temp-inplace))
      (local-file (file-relative-name temp-file (file-name-directory buffer-file-name)))
    )
      (list "hslint" (list local-file))
  )
)

(defun flymake-haskell-enable ()
  "Enables flymake-mode for haskell, and sets as command to show current error."
  (when
    (and buffer-file-name (file-writable-p (file-name-directory buffer-file-name)) (file-writable-p buffer-file-name))
    (local-set-key (kbd "C-c d") 'flymake-display-err-menu-for-current-line)
    (flymake-mode t)
  )
)

;; Forces flymake to underline bad lines, instead of fully
;; highlighting them; remove this if you prefer full highlighting.
(custom-set-faces
  '(flymake-errline ((((class color)) (:underline "red"))))
  '(flymake-warnline ((((class color)) (:underline "yellow"))))
  )

;; binds C-c l to hs-lint inside of Haskell buffers
(defun my-haskell-mode-hook ()
  "hs-lint binding, plus autocompletion and paredit."
  (local-set-key "\C-cl" 'hs-lint)
  (setq ac-sources (append
    '(ac-source-yasnippet
      ac-source-abbrev
      ac-source-words-in-buffer
      my/ac-source-haskell)
    ac-sources
  ))
  (dolist
    (x '(haskell literate-haskell))
    (add-hook (intern (concat (symbol-name x) "-mode-hook")) 'turn-on-paredit)
  )
)

;; Configures a large number of Haskell keywords for autocompletion.
;; TODO maybe obsolete because it triggers an error
;; "File mode specification error: (void-function my-haskell-mode-hook)"
;; (eval-after-load
;;   'haskell-mode
;;   '(progn
;;     (require 'flymake)
;;     (push '("\.l?hs\'" flymake-haskell-init) flymake-allowed-file-name-masks)
;;     (add-hook 'haskell-mode-hook 'flymake-haskell-enable)
;;     (add-hook 'haskell-mode-hook 'my-haskell-mode-hook)
;;   )
;; )
;;END HASKELL______________________________________________________________________________

;; Start merlin on ocaml files
(add-hook 'tuareg-mode-hook 'merlin-mode t)
(add-hook 'caml-mode-hook 'merlin-mode t)

;; Enable auto-complete
(setq merlin-use-auto-complete-mode 't)

;; Use opam switch to lookup ocamlmerlin binary
(setq merlin-command 'opam)

;; color twin parentheses with the same color
;;(require 'highlight-parentheses)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2e3436" "#a40000" "#4e9a06" "#c4a000" "#204a87" "#5c3566" "#729fcf" "#eeeeec"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (tsdh-dark)))
 '(inhibit-startup-screen t)
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(mouse-wheel-progressive-speed nil))
