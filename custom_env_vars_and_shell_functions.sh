function on_laptop() {
  hostname | grep some-laptop > /dev/null
  echo "[ $? -eq 0 ]"
}

if $(on_laptop); then
  export CLOUD_DIR='/home/datumoj/cloud/'
else # desktop
  export CLOUD_DIR='/home/datumoj/cloud'
fi
