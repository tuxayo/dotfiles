# Setup when configuring a new machine
## Shell
In your `.zshrc` add

```
source ~/.config/dotfiles/.zshrc.sh
```

zsh only stuff should be ran only when the shell is zsh.
Which means this should also work with bash.

## Setup: symlink stuff
### Git
```
ln -s ~/.config/dotfiles/DOTgitignore ~/.gitignore
ln -s ~/.config/dotfiles/.gitconfig ~/.gitconfig
```

### Emacs/Spacemacs
#### .spacemacs
`ln -s ~/.config/dotfiles/spacemacs/.spacemacs ~/.spacemacs`
#### robot-framework custom layer
`ln -s ~/.config/dotfiles/spacemacs/private/robot-framework ~/.emacs.d/private/robot-framework`

### Vim
symlink .vimrc
`ln -s ~/.config/dotfiles/.vimrc ~/.vimrc`

### htop
`ln -s ~/.config/dotfiles/htoprc ~/.config/htop/htoprc`
